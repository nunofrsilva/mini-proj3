# MINI PROJ 3 - Animalec back-end

### Proposta
A tarefa 4.2 corresponde ao desenvolvimento inicial do back-end, onde para além de criar a aplicação no Express visa o processamento dos formulários no lado do servidor com os aspetos de validação e pedidos GET e POST. Todo esta implementação de processamentos de pedidos implica a definição de rotas.

Fases da tarefa:

a) Cada aluno deve ler os recursos partilhados para consolidar os conhecimentos JavaScript, Node.js e Express, e compreender a estrutura do código partilhado. Para esta fase da tarefa ser considerada terminada, deverá ter o back-end de processamento de formulários esteja totalmente funcional baseado nas boas práticas nesta componente do full-stack.

b) Cada aluno deve implementar ao nível do back-end o tratamento de pedidos relativos às novas entidades desenvolvidas na tarefa 3.2, com todas as funcionalidades previstas.

c) Cada aluno deve explorar a aplicação Postman para validar os pedidos ao servidor.

d) Todos os ficheiros desenvolvidos devem ser submetidos na plataforma Git, na conta individual de cada aluno. O projeto no Git deve-se chamar de mini-proj3. A avaliação do projeto será realizada a partir dos ficheiros existentes no Git.


### Desenvolvimento
Para a realização  deste projeto foi utilizado o código javascript, correspondente ao back-end da aplicação  disponibilizada no livro recomendado, Desenvolvimento Avançado para a Web - Do front-end ao back-end. 1ª edição, Filipe Portela, Ricardo Queirós, 2020.

Ao código disponibilizado foi adicionado duas entidades, sendo estas a Sponsor e Expert.

Para o desenvolvimento destas funcionalidades foi necessário adicionar e alterar alguns ficheiros como:

#### Sponsor
- sponsor.model.js - Representa o modelo da entidade
- sponsor.controller.js - É o controlador da entidade, responsável por receber os pedidos realizados a rota /sponsors
- sponsor.messages.js - Ficheiro utilizado para configurar as mensagens e códigos http que serão retornados a cada solicitação efetuada ao controlador
- sponsor.routes.js - Ficheiro utilizado para configurar as rotas e métodos aceites pela entidade sponsor

#### Expert
- expert.model.js - Representa o modelo da entidade
- expert.controller.js - É o controlador da entidade, responsável por receber os pedidos realizados na rota /experts
- expert.messages.js - Ficheiro utilizado para configurar as mensagens e códigos http que serão retornados a cada solicitação efetuada ao controlador
- expert.routes.js - Ficheiro utilizado para configurar as rotas e métodos aceites pela entidade expert

- Alteração do ficheiro router.js de modo a incluir as rotas previamente configurados nos ficheiros sponsor.routes.js e expert.routes.js

# Documentação Postman
Na pasta documentation, na raiz do projeto tem disponível um ficheiro JSON que poderá ser importado para o Postman, de modo a aceder à documentação da API, e testar os novos
endpoints que suportam as operações possíveis de realizar às entidades sponsor e expert
